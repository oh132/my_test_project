package com.example.yeonjuoh.indoor_nav_1;

import android.support.v4.view.*;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.MotionEvent;
import android.widget.Toast;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.yeonjuoh.indoor_nav_1.SimpleGestureFilter.SimpleGestureListener;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements SimpleGestureListener, OnInitListener{

    private SimpleGestureFilter detector;
    private TextToSpeech myTTS;
    private final static int NumofInst = 4;

    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        detector = new SimpleGestureFilter(this, this);
        myTTS = new TextToSpeech(this, this);

        viewPager = (ViewPager)findViewById(R.id.pager);
        //fragment pager adapter
        CustomAdapter adapter = new CustomAdapter(getLayoutInflater());
        viewPager.setAdapter(adapter);
    }

    public void onInit(int initStatus){
        if(initStatus == TextToSpeech.SUCCESS)
            myTTS.setLanguage(Locale.US);
        else if(initStatus == TextToSpeech.ERROR)
            Toast.makeText(this, "Sorry! Text To Speech Failed...", Toast.LENGTH_LONG).show();
    }

    public void onSwipe(int direction){
            String str = "";

        switch (direction){
            case SimpleGestureFilter.SWIPE_RIGHT : {
                str = "Swipe Right";
            }
                break;
            case SimpleGestureFilter.SWIPE_LEFT : {
                str = "Swipe Left";
            }
                break;
        }

        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
//        myTTS.speak(InstArray[count], TextToSpeech.QUEUE_FLUSH, null);
    }

    public void onDoubleTap(){
        Toast.makeText(this, "Double Tap", Toast.LENGTH_SHORT).show();
//        myTTS.speak(InstArray[count], TextToSpeech.QUEUE_FLUSH, null);
    }

    public void mOnClick(View v){

        int position;

        switch( v.getId() ){
            case R.id.btn_previous://이전버튼 클릭

                position=viewPager.getCurrentItem();//현재 보여지는 아이템의 위치를 리턴

                //현재 위치(position)에서 -1 을 해서 이전 position으로 변경
                //이전 Item으로 현재의 아이템 변경 설정(가장 처음이면 더이상 이동하지 않음)
                //첫번째 파라미터: 설정할 현재 위치
                //두번째 파라미터: 변경할 때 부드럽게 이동하는가? false면 팍팍 바뀜
                viewPager.setCurrentItem(position-1,true);

                break;

            case R.id.btn_current:

                break;

            case R.id.btn_next://다음버튼 클릭

                position=viewPager.getCurrentItem();//현재 보여지는 아이템의 위치를 리턴

                //현재 위치(position)에서 +1 을 해서 다음 position으로 변경
                //다음 Item으로 현재의 아이템 변경 설정(가장 마지막이면 더이상 이동하지 않음)
                //첫번째 파라미터: 설정할 현재 위치
                //두번째 파라미터: 변경할 때 부드럽게 이동하는가? false면 팍팍 바뀜
                viewPager.setCurrentItem(position+1,true);

                break;
        }

    }
}
