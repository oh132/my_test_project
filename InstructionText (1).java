package com.example.yeonjuoh.indoor_nav_1;

/**
 * Created by Yeonju Oh on 9/9/2016.
 */

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class InstructionText {
    public static final String[] InstTexts = {"1", "2", "3", "4"};
    public static final Map<String, String> TEXT_DETAIL;

    static{
        Map<String, String> Instructions = new HashMap<String, String>();
        Instructions.put("1", "Touch the wall on your right side.");
        Instructions.put("2", "Go straight until the corner.");
        Instructions.put("3", "Turn right.");
        Instructions.put("4", "Go straight. Then there is the destination in front of you.");

        TEXT_DETAIL = Collections.unmodifiableMap(Instructions);
    }
}
