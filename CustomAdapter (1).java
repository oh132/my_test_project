package com.example.yeonjuoh.indoornav_test.;

import android.support.v4.view.PagerAdapter;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Yeonju Oh on 9/9/2016.
 */
public class CustomAdapter extends PagerAdapter {
    LayoutInflater inflater;

    public CustomAdapter(LayoutInflater inflater){
        this.inflater = inflater;
    }

    public int getCount(){
        return 4;
    }

    public Object instantiateItem(ViewGroup container, int position){
        View view = null;
        view = inflater.inflate(R.layout.viewpager_childview, null);

        TextView textView = (TextView)view.findViewById(R.id.instruction_text+position);
        String inst = InstructionText.InstTexts[position];
        textView.setText("Current Step");
        container.addView(view);
        return view;
    }

    public void destroyItem(ViewGroup container, int position, Object object){
        container.removeView((View)object);
    }

    public boolean isViewFromObject(View v, Object obj){
        return v==obj;
    }
}
